This image uses GDK Broadway instead of noVNC. This means very little lag, even with many search results, and no fixed resolution in the browser.

Don't forget to forward the ports for Soulseek on your router. Check the Nicotine settings.

```
docker run -it
    -p 8080:8080 \
    -v /my-downloads:/downloads \
    -e LOGIN=username \
    -e PASSW=password \
    -e DARKMODE=true \
    freddywullockx/nicotineplus:latest
```

Docker-compose template:

```
  nicotineplus:
    container_name: nicotineplus
    image: freddywullockx/nicotineplus
    restart: unless-stopped
    environment:
      - LOGIN=username
      - PASSW=password
      - DARKMODE=true
    ports:
      - 6080:8080
    volumes:
      - /my-downloads:/downloads
```